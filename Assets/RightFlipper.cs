﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightFlipper : MonoBehaviour
{
    public Rigidbody rigidboidy;

    void Start()
    {
        rigidboidy.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rigidboidy.AddForce(new Vector3(0, 0, -150), ForceMode.Impulse);
        }
        else
        {
            rigidboidy.AddForce(new Vector3(0, 0, 150), ForceMode.Impulse);
        }
    }
}
