﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static int score = 0;
    public TextMesh display;

    void Update()
    {
        if (display)
        {
            display.text = "Score "+score.ToString("D8");
        }
    }
}
