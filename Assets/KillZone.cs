﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    public Transform Spawn;
    public GameObject Ball;
    
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Ball")
        {
            if (BallLives.lives >= 0)
            {
                Ball.transform.position = new Vector3(Spawn.transform.position.x, Spawn.transform.position.y, Spawn.transform.position.z);
                BallLives.lives--;
            }
            if(BallLives.lives< 0)
            {
                Ball.SetActive(false);
            }
        }
    }
}
