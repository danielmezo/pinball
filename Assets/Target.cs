﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public static float dropDistance = 1.0f;
    public   int bankID = 0;
    public  float resetDelay = 0.5f;
    public static List<Target> dropTargets = new List<Target>();
    public  int targetValue = 100;
    public  int bankValue = 5000;
    public Light m_light;

    public bool isDropped = false;

    // Use this for initialization
    void Start()
    {
        dropTargets.Add(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter()
    {
        if (!isDropped)
        {
            transform.position += Vector3.down * dropDistance;
            isDropped = true;
            Score.score += targetValue;
            m_light.enabled = true;

            bool resetBank = true;
            foreach (Target target in dropTargets)
            {
                if (target.bankID == bankID)
                {
                    if (!target.isDropped)
                    {
                        resetBank = false;
                    }
                }
            }
            if (resetBank)
            {
                Score.score += bankValue;
                Invoke("ResetBank", resetDelay);
            }
        }
    }

    public void ResetBank()
    {
        foreach (Target target in dropTargets)
        {
            if (target.bankID ==bankID)
            {
                target.transform.position += Vector3.up * dropDistance;
                target.isDropped = false;
                m_light.enabled = false;
            }
            if (BallLives.gameOver == true)
            {
                target.transform.position += Vector3.up * dropDistance;
                target.isDropped = false;
                m_light.enabled = false;
            }
        }
    }
}
