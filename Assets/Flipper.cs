﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    public Rigidbody rigidboidy;

    void Start()
    {
        rigidboidy.GetComponent<Rigidbody>();
    }
    
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            rigidboidy.AddForce(new Vector3(0, 0, -150), ForceMode.Impulse);
        }
        else
        {
            rigidboidy.AddForce(new Vector3(0, 0, 150), ForceMode.Impulse);
        }
    }
}
