﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallLives : MonoBehaviour {

    public GameObject ball;
    public Transform spawnPosition;
    public TextMesh display;
    public static int lives = 3;
    public static bool gameOver = false;

    void Update()
    {
        if (lives < 0)
        {
            if (display)
            {
                display.text = "GAME OVER";
                gameOver = true;
            }
        }
        if (lives>=0 && gameOver==false)
        {
            if (display)
            {
                display.text = "BALLS: " + lives.ToString();
            }
        }
    }
}
