﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spring : MonoBehaviour
{
    public string inputButtonName="Submit";
    public float m_Distance = 1.8f;
    public float m_Speed = 1.5f;
    public GameObject m_Ball;
    public float m_Power = 2000.0f;
    public Transform Position;

    public bool ready = false;
    public bool fire = false;
    private float moveCount = 0.0f;
    private Vector3 m_Start;

	void Start()
    {

	}
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            ready = true;
        }
    }
    void FixedUpdate()
    {
        if (Input.GetButton(inputButtonName))
        {
            if (moveCount < m_Distance)
            {
                transform.Translate(m_Speed * Time.deltaTime, 0, 0);
                moveCount += m_Speed * Time.deltaTime;
                fire = true;
            }
        }
        else if (moveCount>0)
        {
            if (fire && ready)
            {
                m_Ball.transform.TransformDirection(Vector3.forward * 10);
                m_Ball.GetComponent<Rigidbody>().AddForce(0, 0, -(moveCount * m_Power));
                fire = false;
                ready = false;
            }
            transform.Translate(-20 * Time.deltaTime, 0, 0);
            moveCount -= 20 * Time.deltaTime;
        }
        if (moveCount<=0)
        {
            fire = false;
            moveCount = 0;
        }
    }
}
