﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bumper : MonoBehaviour 
{
	public float force = 100.0f;
	public float forceRadius = 1.0f;
    public Light Light;
    bool active = false;
	void Start() 
	{

	}
	void Update() 
	{
	}
	void OnCollisionEnter(Collision collision)
	{
		foreach(Collider col in Physics.OverlapSphere(transform.position, forceRadius))
		{
			if(col.GetComponent<Rigidbody>())
			{
				col.GetComponent<Rigidbody>().AddExplosionForce(force, transform.position, forceRadius);
			}
            if(collision.gameObject.tag == "Ball")
            {
                Light.enabled = true;
                Score.score += 100;
                active = true;
            }
        }
    }
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            Light.enabled = false;
        }
    }

}
